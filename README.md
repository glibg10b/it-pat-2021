# Grade 11 IT PAT

## Features

- 5 ciphers, including one I made myself
- Ability to copy text to/from clipboard
- Ability to en-/decrypt files
- Toggleable dark mode
- A help window
- A window with a progress bar
- Remembers your last settings

## User interface

### Main window

![ROT-13 and its settings](res/rot-13.png)

![Caesar cipher and its settings](res/caesar.png)

![Waldo's shuffling cipher and its settings](res/waldos-shuffling.png)

![Simple substitution cipher and its settings](res/simple-substitution.png)

![One-time pad and its settings](res/otp.png)

![Dark mode toggle](res/dark-mode.png)

### Help window

![Section 1](res/help-1.png)

![Section 2](res/help-2.png)

### Loading window

![A small window with a progress bar](res/loading-window.png)
