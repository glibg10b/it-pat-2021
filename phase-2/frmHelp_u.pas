// The help screen that shows up when clicking on "Help" in the main form's menu
unit frmHelp_u;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ComCtrls;

type
  TfrmHelp = class(TForm)
    redBody: TRichEdit;
    cbxSection: TComboBox;
    btnFirst: TButton;
    btnPrev: TButton;
    btnNext: TButton;
    btnLast: TButton;
    procedure FormShow(Sender: TObject);
    procedure btnFirstClick(Sender: TObject);
    procedure setPage(iPage: integer);
    procedure cbxSectionChange(Sender: TObject);
    procedure btnLastClick(Sender: TObject);
    procedure btnPrevClick(Sender: TObject);
    procedure btnNextClick(Sender: TObject);
  private
    { Private declarations }
  public
    arrHeadings, arrBodies: array of string;
  end;

var
  frmHelp: TfrmHelp;

implementation

{$R *.dfm}

// Select a different page
procedure TfrmHelp.setPage(iPage: integer);
begin
  redBody.clear;
  redBody.lines.add(arrBodies[iPage]);
  cbxSection.itemIndex := iPage;
  if cbxSection.itemIndex = low(arrHeadings) then
  begin
    btnFirst.enabled := false;
    btnPrev.enabled  := false;
    btnNext.enabled  := true;
    btnLast.enabled  := true;
  end
  else if cbxSection.ItemIndex = high(arrHeadings) then
  begin
    btnFirst.enabled := true;
    btnPrev.enabled  := true;
    btnNext.enabled  := false;
    btnLast.enabled  := false;
  end
  else
  begin
    btnFirst.enabled := true;
    btnPrev.enabled  := true;
    btnNext.enabled  := true;
    btnLast.enabled  := true;
  end;
end;

// Go to the first page
procedure TfrmHelp.btnFirstClick(Sender: TObject);
begin
  setPage(low(arrHeadings));
end;

// Go to the last page
procedure TfrmHelp.btnLastClick(Sender: TObject);
begin
  setPage(high(arrHeadings));
end;

// Go to the next page
procedure TfrmHelp.btnNextClick(Sender: TObject);
begin
  if cbxSection.itemIndex < high(arrHeadings) then
    setPage(cbxSection.itemIndex + 1);
end;

// Go the the previous page
procedure TfrmHelp.btnPrevClick(Sender: TObject);
begin
  if cbxSection.itemIndex > low(arrHeadings) then
    setPage(cbxSection.itemIndex - 1);
end;

// Choose a different page
procedure TfrmHelp.cbxSectionChange(Sender: TObject);
begin
  setPage(cbxSection.ItemIndex);
end;

// Fill the pages and sections
procedure TfrmHelp.FormShow(Sender: TObject);
var
  i: integer;
begin
  for i := low(arrHeadings) to high(arrHeadings) do
    cbxSection.items[i] := arrHeadings[i];
end;

end.
