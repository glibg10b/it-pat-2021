// File input/output functions
unit io_u;

interface

// Open an import/export dialog
// bSave = true for export, false for import
// Returns true if user chose a file
// Throws error if file can't be acquired
function portTextFile(var tFile: textFile; bSave: boolean): boolean;

implementation

uses Vcl.Dialogs;

function portTextFile(var tFile: textFile; bSave: boolean): boolean;
var
  sFile: string;
  sTitle: string;
begin
  if bSave then sTitle := 'Export file...'
           else sTitle := 'Import file...';

  result := promptForFileName(
    sFile,
    'Text Documents (*.txt)|*.txt|All Files (*.*)|*.*',
    '.txt',
    sTitle,
    '', // Last initial directory
    bSave);

  if result then
  begin
    assignFile(tFile, sFile);
    if bSave then rewrite(tFile) // Rely on the caller to catch an error
             else reset(tFile);
  end;
end;

end.
