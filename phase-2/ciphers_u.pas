// The five ciphers

unit ciphers_u;

interface

uses Vcl.ComCtrls;

const
  DEFAULT_OFFSET    = 16;      // Used by Caesar cipher
  DEFAULT_SEED      = 7355608; // Used by Waldo's shuffling cipher

  // Used by mono-alphabetic substitution cipher
  DEFAULT_ALPHAKEY  = 'B 90$''3hg~ifa?P<2:oeRAv[(%F^#MLEZ;*1N=!mC85yXrw6z_`|{JG'
    + 'TVD]&xIjnds>7)Kc,4S-u/Y}@p"UW.qQkl\OtHb+';

  // Used by one-time pad
  DEFAULT_SECUREKEY = 'Using the default key is pointless, use your own.';

type
  // Cipher
  TCipher = (cipher_rot13 = 0, cipher_caesar, cipher_shuf, cipher_subst,
    cipher_otp); // cipher_ prefixes to avoid naming collisions

  // Encrypt or decrypt
  TCrypt = (crypt_encrypt = 0, crypt_decrypt);

// En/decrypt sInput with ROT-13
procedure rot13(var sInput: string);

// En/decrypt sInput with Caesar Cipher
procedure caesar(var sInput: string;                 // Input string
                 crypt: TCrypt;                      // Encrypt/decrypt
                 iOffset: integer = DEFAULT_OFFSET); // Offset

// En/decrypt sInput with Waldo's shuffling cipher
procedure shufCipher(var sInput: string; crypt: TCrypt;
                     iSeed: integer = DEFAULT_SEED); // Seed

// En/decrypt sInput with mono-alphabetic substitution cipher
procedure subst(var sInput: string; crypt: TCrypt;
                sAlphabet: string = DEFAULT_ALPHAKEY); // Shuffled alphabet

// En/decrypt sInput with one-time pad                
procedure otp(var sInput: string;
              sKey: string = DEFAULT_SECUREKEY); // Secure key

implementation

uses Vcl.Dialogs,     // showMessage()
     strmanip_u,      // rot()
     algorithms_u,    // quickSort()
     Forms,           // Application.ProcessMessages
     frmLoading_u;    // pgbProgress

// rotate every letter by 13
procedure rot13(var sInput: string);
begin
  // encrypt/decrypt doesn't matter because 26 - 13 = 13
  caesar(sInput, crypt_encrypt,  13);
end;

// rotate every letter in sInput by iOffset when encrypting
//                                  26 - iOffset when decrypting
procedure caesar(var sInput: string; crypt: TCrypt;   iOffset: integer);
var i: integer;
begin
  if crypt = crypt_decrypt then iOffset := 26 - iOffset;

  for i := low(sInput) to high(sInput) do
  begin
    rot(sInput[i], iOffset);

    frmLoading.pgbProgress.stepIt;
    application.ProcessMessages;
  end;
end;

// Seed the randomizer with iSeed then rotate each char with a random offset.
// Delphi's PRNG is deterministic, so the result is the same every time for a
// given seed.
procedure shufCipher(var sInput: string; crypt: TCrypt;   iSeed: integer);
var iLetter: integer;
begin
  defaultRandomize(iSeed);
  random; // Discard the first random number because it isn't random

  // Putting the random numbers in an array first isn't necessary, they can be
  // used directly
  for iLetter := low(sInput) to high(sInput) do
  begin
    if crypt = crypt_encrypt then rot(sInput[iLetter], random(26))
                             else rot(sInput[iLetter], 26 - random(26));

    frmLoading.pgbProgress.stepIt;
    application.ProcessMessages;
  end;

  randomize; // So random numbers are unpredictable again
end;

// Validate sAlphabet then substitute each letter with its shuffled equivalent
procedure subst(var sInput: string; crypt: TCrypt;   sAlphabet: string);
const
  PLAINTEXT_ALPHABET = ' !"#$%&''()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTU'
    + 'VWXYZ[\]^_`abcdefghijklmnopqrstuvwxyz{|}~';
var i, iPos: integer;
begin
  if length(sAlphabet) <> length(PLAINTEXT_ALPHABET) then
  begin
    showMessage('Key must be 95 characters long');
    exit;
  end;

  if quicksort(sAlphabet,
               low(sAlphabet),
               high(sAlphabet)) <> PLAINTEXT_ALPHABET then
  begin
    showMessage('Invalid key -- see cipher info');
    exit;
  end;

  for i := low(sInput) to high(sInput) do
  begin
    if crypt = crypt_encrypt then
    begin
      iPos := pos(sInput[i], PLAINTEXT_ALPHABET);
      if iPos <> 0 then sInput[i] := sAlphabet[iPos];
    end
    else
    begin
      iPos := pos(sInput[i], sAlphabet);
      if iPos <> 0 then sInput[i] := PLAINTEXT_ALPHABET[iPos];
    end; // if crypt = crypt_encrypt

    frmLoading.pgbProgress.stepIt;
    application.ProcessMessages;
  end; // for i := low(sInput) to high(sInput)
end;

// Bitwise xor each input character with each character in the secure key
// Secure key wraps around if it's too short
procedure otp(var sInput: string; sKey: string);
var i, iKeyPos: integer;
begin
  iKeyPos := low(sKey);
  for i := low(sInput) to high(sInput) do
  begin  
    // Subtracting then adding 32 avoids producing control characters
    // (see ASCII table)
    sInput[i] := char( 
      (ord(sInput[i]) - 32) xor (ord(sKey[iKeyPos]) - 32) + 32
      );
      
    if iKeyPos < high(sKey) then inc(iKeyPos) else iKeyPos := low(sKey);

    frmLoading.pgbProgress.stepIt;
    application.ProcessMessages;
  end;
end;

end.
