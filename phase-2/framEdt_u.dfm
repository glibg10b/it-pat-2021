object framEdt: TframEdt
  Left = 0
  Top = 0
  Width = 510
  Height = 52
  TabOrder = 0
  DesignSize = (
    510
    52)
  object edt: TLabeledEdit
    Left = 3
    Top = 20
    Width = 341
    Height = 21
    Anchors = [akLeft, akRight]
    EditLabel.Width = 16
    EditLabel.Height = 13
    EditLabel.Caption = 'edt'
    PasswordChar = '*'
    TabOrder = 0
    OnExit = edtExit
  end
  object btnShow: TButton
    Left = 350
    Top = 16
    Width = 28
    Height = 25
    Hint = 'Show'
    Anchors = [akRight]
    Caption = #55357#56385
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnMouseDown = btnShowMouseDown
    OnMouseUp = btnShowMouseUp
  end
  object btnCopy: TButton
    Left = 384
    Top = 16
    Width = 26
    Height = 25
    Hint = 'Copy'
    Anchors = [akRight]
    Caption = #55357#56778
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    OnClick = btnCopyClick
  end
  object btnPaste: TButton
    Left = 416
    Top = 16
    Width = 26
    Height = 25
    Hint = 'Paste'
    Anchors = [akRight]
    Caption = #55357#56523#65038
    ParentShowHint = False
    ShowHint = True
    TabOrder = 3
    OnClick = btnPasteClick
  end
  object btnImport: TButton
    Left = 448
    Top = 16
    Width = 26
    Height = 25
    Hint = 'Import file...'
    Anchors = [akRight]
    Caption = #55357#56769
    ParentShowHint = False
    ShowHint = True
    TabOrder = 4
    OnClick = btnImportClick
  end
  object btnExport: TButton
    Left = 480
    Top = 16
    Width = 27
    Height = 25
    Hint = 'Export file...'
    Anchors = [akRight]
    Caption = #55357#56747
    ParentShowHint = False
    ShowHint = True
    TabOrder = 5
    OnClick = btnExportClick
  end
end
