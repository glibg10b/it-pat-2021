unit en_decryptor_u;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons, Vcl.Menus, Vcl.Themes, Vcl.Styles,
  Vcl.ComCtrls, Vcl.Samples.Spin,

  frmLoading_u, framEdt_u, ciphers_u;

const
  CONFIGFILE_NAME = '.cfg';
  HELPFILE_NAME = '.help';

type
  TfrmMain = class(TForm)
    rgpMode: TRadioGroup;
    mainMenu: TMainMenu;
    view: TMenuItem;
    darkmode: TMenuItem;
    help: TMenuItem;
    btnStart: TButton;
    cbxCipher: TComboBox;
    redCipherInfo: TRichEdit;
    pnlOpts: TPanel;
    rgpOpts: TRadioGroup;
    sedNum: TSpinEdit;
    bmpGenerate: TBitBtn;
    fedIn: TframEdt;
    fedOut: TframEdt;
    fedOpt: TframEdt;
    procedure changeCipher(cipher: TCipher);
    procedure changeCrypt(crypt: TCrypt);
    procedure changeOpts(bCustom: boolean);
    procedure changeGUI(sRgpOptsCaption: string = '';
                        sSedNumHint: string = '';
                        sfedOptCaption: string = '';
                        sBmpGenerateCaption: string = '');
    procedure changeDescription(sDesc: string);
    procedure changeOffset(iO: integer);
    procedure changeSeed(iSd: integer);
    procedure changeKey(sK: string);
    procedure changeSecureKey(sSK: string);
    procedure writeConfig();
    procedure readConfig();
    procedure readHelp();
    procedure btnStartClick(Sender: TObject);
    procedure cbxCipherChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure helpClick(Sender: TObject);
    procedure bmpGenerateClick(Sender: TObject);
    procedure rgpOptsClick(Sender: TObject);
    procedure rgpModeClick(Sender: TObject);
    procedure fedOptedtExit(Sender: TObject);
    procedure sedNumExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure darkmodeClick(Sender: TObject);
  private
    iOffset:    integer;
    iSeed:      integer;
    sKey:       string;
    sSecureKey: string;
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

const
  // Cipher names
  CIPHER_NAMES: array [TCipher] of string = ('ROT-13', 'Caesar cipher',
    'Waldo''s shuffling cipher', 'Simple substitution cipher', 'One-time pad');

implementation

{$R *.dfm}

uses System.UITypes, frmHelp_u, strmanip_u, algorithms_u, Winapi.ShellAPI;

// ------------------------- GUI changing functions ------------------------- //

// Helper function for changeCipher()
// Empty argument -> hidden component
procedure TfrmMain.changeGUI(sRgpOptsCaption: string = '';
                             sSedNumHint: string = '';
                             sfedOptCaption: string = '';
                             sBmpGenerateCaption: string = '');
begin
  rgpOpts.caption := sRgpOptsCaption;
  rgpOpts.visible := not empty(sRgpOptsCaption);

  sedNum.Hint    := sSedNumHint;
  sedNum.visible := not empty(sSedNumHint);

  fedOpt.edt.editLabel.caption := sfedOptCaption;
  fedOpt.visible               := not empty(sfedOptCaption);

  bmpGenerate.caption := sBmpGenerateCaption;
  bmpGenerate.visible := not empty(sBmpGenerateCaption);
end;

// Change GUI according to cipher
procedure TfrmMain.changeCipher(cipher: TCipher);
begin
  case cipher of
    cipher_rot13:
    begin
      changeGUI();
      changeDescription(frmHelp.arrBodies[2]);
    end;
    cipher_caesar:
    begin
      changeGUI('Offset', 'Enter an offset');
      sedNum.minValue := 1;
      sedNum.maxValue := 25;
      changeOffset(iOffset);
      changeDescription(frmHelp.arrBodies[3]);
    end;
    cipher_shuf:
    begin
      changeGUI('Seed', 'Enter a seed');
      sedNum.minValue := 0;
      sedNum.maxValue := 0;
      changeSeed(iSeed);
      changeDescription(frmHelp.arrBodies[4]);
    end;
    cipher_subst:
    begin
      changeGUI('Key', '', '95-character alphanumeric key', 'Generate');
      fedOpt.edt.maxLength := 95;
      changeKey(sKey);
      changeDescription(frmHelp.arrBodies[5]);
    end;
    cipher_otp:
    begin
      changeGUI('Secure key (OTP)', '', 'Alphanumeric secure key (OTP)',
        'Generate');
      // OTP doesn't have to be as long as the input, so it can be reused for
      // inputs of varying lengths.
      fedOpt.edt.maxLength := 0;
      changeSecureKey(sSecureKey);
      changeDescription(frmHelp.arrBodies[6]);
    end;
  end;

  cbxCipher.itemIndex := ord(cipher);
end;

// Change GUI according to crypt (en-/decrypt)
procedure TfrmMain.changeCrypt(crypt: TCrypt);
begin
  case crypt of
    crypt_encrypt:
    begin
      fedIn.edt.editLabel.caption  := 'Plain text';
      fedOut.edt.editLabel.caption := 'Cipher text';
      btnStart.caption             := 'Encrypt';
    end;
    crypt_decrypt:
    begin
      fedIn.edt.editLabel.caption  := 'Cipher text';
      fedOut.edt.editLabel.caption := 'Plain text';
      btnStart.caption             := 'Decrypt';
    end;
  end;

  rgpMode.itemIndex := ord(crypt);
end;
// Change description to sDesc
procedure TfrmMain.changeDescription(sDesc: string);
begin
  redCipherInfo.clear;
  redCipherInfo.lines.add(sDesc);
end;

// Change GUI according to bCustom (custom/default)
procedure TfrmMain.changeOpts(bCustom: boolean);
begin
  rgpOpts.ItemIndex   := ord(bCustom);
  sedNum.enabled      := bCustom;
  bmpGenerate.enabled := bCustom;
  fedOpt.enable(         bCustom );
end;

// Change GUI according to iO (Caesar cipher's offset)
procedure TfrmMain.changeOffset(iO: integer);
begin
  iOffset := iO;
  if cbxCipher.itemIndex = ord(cipher_caesar) then sedNum.value := iO;
end;

// Change GUI according to iSd (Waldo's shuffling cipher's seed)
procedure TfrmMain.changeSeed(iSd: integer);
begin
  iSeed := iSd;
  if cbxCipher.itemIndex = ord(cipher_shuf) then sedNum.value := iSd;
end;

// Change GUI according to sK (substitution cipher's key)
procedure TfrmMain.changeKey(sK: string);
begin
  sKey := sK;
  if cbxCipher.itemIndex = ord(cipher_subst) then fedOpt.setText(sK);
end;

// Change GUI according to sSK (otp's secure key)
procedure TfrmMain.changeSecureKey(sSK: string);
begin
  sSecureKey := sSK;
  if cbxCipher.itemIndex = ord(cipher_otp) then fedOpt.setText(sSK);
end;

// -------------------------------------------------------------------------- //

// -------------------------- GUI changing events --------------------------- //

// Change crypt (en-/decrypt)
procedure TfrmMain.rgpModeClick(Sender: TObject);
begin
  changeCrypt(TCrypt(rgpMode.ItemIndex));
end;

// Change default/custom
procedure TfrmMain.rgpOptsClick(Sender: TObject);
begin
  changeOpts(bool(rgpOpts.itemIndex));
end;

// Change cipher
procedure TfrmMain.cbxCipherChange(Sender: TObject);
begin
  changeCipher(TCipher(cbxCipher.itemIndex));
end;

// Save the contents of sedNum
procedure TfrmMain.sedNumExit(Sender: TObject);
begin
  case TCipher(cbxCipher.itemIndex) of
    cipher_caesar: changeOffset(sedNum.Value);
    cipher_shuf:   changeSeed(  sedNum.Value);
  end;
end;

// Save the contents of fedOpt.edt
procedure TfrmMain.fedOptedtExit(Sender: TObject);
begin
  case TCipher(cbxCipher.itemIndex) of
    cipher_subst:  changeKey(      fedOpt.edt.text);
    cipher_otp:    changeSecureKey(fedOpt.edt.text);
  end;

  fedOpt.edtExit(Sender);
end;

// Toggle dark mode, save and restart
procedure TfrmMain.darkmodeClick(Sender: TObject);
begin
  darkmode.checked := not darkmode.checked;
  writeConfig();

  // Restart application
  ShellExecute(Handle, nil, PChar(Application.ExeName), nil, nil,
    SW_SHOWNORMAL);
  application.terminate;
end;

// -------------------------------------------------------------------------- //

// ------------------------------- Config file ------------------------------ //

// Write settings to CONFIGFILE_NAME
procedure TfrmMain.writeConfig();
var
  tConfigFile: textFile;
begin
  assignFile(tConfigFile, CONFIGFILE_NAME);
  try rewrite(tConfigFile) except
    messageDlg('Failed to save settings to ' + CONFIGFILE_NAME, mtError, [mbOK],
      0);
  end;

  writeln(tConfigFile, cbxCipher.itemIndex);
  writeln(tConfigFile, rgpOpts.itemIndex);
  writeln(tConfigFile, iOffset);
  writeln(tConfigFile, iSeed);
  writeln(tConfigFile, sKey);
  writeln(tConfigFile, sSecureKey);
  writeln(tConfigFile, rgpMode.itemIndex);
  writeln(tConfigFile, darkmode.checked);
  
  closeFile(tConfigFile);
end;

// Read settings from CONFIGFILE_NAME if it exists
procedure TfrmMain.readConfig();
var
  tConfigFile: textFile;
  arrConfig: array[1..8] of string;
  i: integer;
begin
  assignFile(tConfigFile, CONFIGFILE_NAME);

  try reset(tConfigFile) except exit end; // Use defaults instead

  try
    for i := low(arrConfig) to high(arrConfig) do
      readln(tConfigFile, arrConfig[i]);

    changeCipher(TCipher(  strToInt(arrConfig[1])));
    changeOpts(            strToBool(arrConfig[2]));
    changeOffset(          strToInt(arrConfig[3]));
    changeSeed(            strToInt(arrConfig[4]));
    changeKey(                      arrConfig[5]);
    changeSecureKey(                arrConfig[6]);
    changeCrypt(TCrypt(    strToInt(arrConfig[7])));
    darkmode.checked :=    strToBool(arrConfig[8]);
  except
    messageDlg(CONFIGFILE_NAME + ' is invalid, ignoring...', mtError, [mbOK],
      0);
  end;

  closeFile(tConfigFile);
end;

// -------------------------------------------------------------------------- //

// -------------------------------- Help file ------------------------------- //

// Read contents of HELPFILE_NAME and store it in frmHelp's arrays
procedure TfrmMain.readHelp();
var
  tHelpFile: textFile;
  i: integer;
  sLine: string;
begin
  assignFile(tHelpFile, HELPFILE_NAME);

  try reset(tHelpFile) except
    messageDlg('Failed to load ' + CONFIGFILE_NAME, mtError, [mbOK],
      0);
    exit;
  end; // Use defaults instead

  with frmHelp do
  begin
    i := -1;
    while not eof(tHelpFile) do
    begin
      readln(tHelpFile, sLine);
      if (not empty(sLine)) and (sLine[1] = '#') then
      begin
        inc(i);
        setLength(arrHeadings, i + 1);
        setLength(arrBodies, i + 1);
        arrHeadings[i] := copy(sLine, 2, 30); // Don't want long headings
      end
      else arrBodies[i] := arrBodies[i] + sLine + #10;
    end;
  end;

  closeFile(tHelpFile);
end;

// -------------------------------------------------------------------------- //

// Prepare form
procedure TfrmMain.FormCreate(Sender: TObject);
var i: TCipher;
begin
  randomize;
  for i := low(TCipher) to high(TCipher) do
    cbxCipher.items[ord(i)] := CIPHER_NAMES[i];
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  readHelp();

  changeCipher(cipher_shuf);
  changeCrypt(crypt_encrypt);
  changeOpts(true); // Custom
  changeOffset(0);
  changeSeed(0);
  changeKey('');
  changeSecureKey('');
  // Dark mode is handled in en_decryptor_p.dpr

  readConfig();
  writeConfig();
end;

// Generate random alphabet/secure key
procedure TfrmMain.bmpGenerateClick(Sender: TObject);
begin
  case TCipher(cbxCipher.ItemIndex) of
    cipher_subst: fedOpt.setText(shuffle(DEFAULT_ALPHAKEY));
    cipher_otp:   fedOpt.setText(randFill(fedIn.edt.text));
  end;
end;

// Show help menu
procedure TfrmMain.helpClick(Sender: TObject);
begin
  frmHelp.showModal;
end;

// Show loading window and start en/decrypting
procedure TfrmMain.btnStartClick(Sender: TObject);
begin
  writeConfig();
  frmLoading.showModal;
end;

end.
