object frmHelp: TfrmHelp
  Left = 0
  Top = 0
  Caption = 'Help'
  ClientHeight = 411
  ClientWidth = 584
  Color = clBtnFace
  Constraints.MinHeight = 450
  Constraints.MinWidth = 600
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    584
    411)
  PixelsPerInch = 96
  TextHeight = 13
  object redBody: TRichEdit
    Left = 8
    Top = 8
    Width = 568
    Height = 368
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelEdges = []
    BevelInner = bvNone
    BevelOuter = bvNone
    BorderStyle = bsNone
    Ctl3D = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentCtl3D = False
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    Zoom = 100
  end
  object cbxSection: TComboBox
    Left = 170
    Top = 384
    Width = 244
    Height = 21
    Anchors = [akBottom]
    ItemIndex = 0
    TabOrder = 1
    Text = 'Section'
    OnChange = cbxSectionChange
    Items.Strings = (
      'Section')
  end
  object btnFirst: TButton
    Left = 8
    Top = 382
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'First'
    TabOrder = 2
    OnClick = btnFirstClick
  end
  object btnPrev: TButton
    Left = 89
    Top = 382
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Previous'
    TabOrder = 3
    OnClick = btnPrevClick
  end
  object btnNext: TButton
    Left = 420
    Top = 382
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Next'
    TabOrder = 4
    OnClick = btnNextClick
  end
  object btnLast: TButton
    Left = 501
    Top = 382
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Last'
    TabOrder = 5
    OnClick = btnLastClick
  end
end
