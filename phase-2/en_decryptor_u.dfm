object frmMain: TfrmMain
  Left = 0
  Top = 0
  Anchors = [akTop]
  Caption = 'En-/decryptor'
  ClientHeight = 541
  ClientWidth = 876
  Color = clBtnFace
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mainMenu
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    876
    541)
  PixelsPerInch = 96
  TextHeight = 13
  object rgpMode: TRadioGroup
    Left = 356
    Top = 408
    Width = 162
    Height = 40
    Anchors = [akBottom]
    Caption = 'Mode'
    Columns = 2
    DoubleBuffered = False
    ItemIndex = 0
    Items.Strings = (
      'Encrypt'
      'Decrypt')
    ParentDoubleBuffered = False
    TabOrder = 0
    OnClick = rgpModeClick
  end
  object btnStart: TButton
    Left = 399
    Top = 454
    Width = 75
    Height = 25
    Anchors = [akBottom]
    Caption = 'Start'
    TabOrder = 4
    OnClick = btnStartClick
  end
  object cbxCipher: TComboBox
    Left = 356
    Top = 198
    Width = 162
    Height = 21
    Style = csDropDownList
    Anchors = [akBottom]
    ParentShowHint = False
    ShowHint = True
    TabOrder = 1
    OnChange = cbxCipherChange
    Items.Strings = (
      'To'
      'be'
      'filled'
      'by'
      'FormCreate()')
  end
  object redCipherInfo: TRichEdit
    Left = 8
    Top = 66
    Width = 860
    Height = 126
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelEdges = []
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'The simple substitution cipher works by ')
    ParentFont = False
    ParentShowHint = False
    ReadOnly = True
    ScrollBars = ssVertical
    ShowHint = True
    TabOrder = 2
    Zoom = 100
  end
  object pnlOpts: TPanel
    Left = 8
    Top = 225
    Width = 860
    Height = 177
    Anchors = [akLeft, akRight, akBottom]
    BevelKind = bkFlat
    BevelOuter = bvNone
    Caption = 'Advanced options:'
    TabOrder = 3
    VerticalAlignment = taAlignTop
    DesignSize = (
      856
      173)
    object rgpOpts: TRadioGroup
      Left = 346
      Top = 22
      Width = 162
      Height = 65
      Anchors = []
      Caption = 'Offset'
      ItemIndex = 1
      Items.Strings = (
        'Default'
        'Custom (recommended):')
      TabOrder = 0
      OnClick = rgpOptsClick
    end
    object sedNum: TSpinEdit
      Left = 389
      Top = 120
      Width = 75
      Height = 22
      Anchors = []
      MaxValue = 0
      MinValue = 0
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      Value = 0
      OnExit = sedNumExit
    end
    object bmpGenerate: TBitBtn
      Left = 389
      Top = 93
      Width = 75
      Height = 25
      Anchors = []
      Caption = '&Generate'
      Kind = bkRetry
      NumGlyphs = 2
      TabOrder = 2
      OnClick = bmpGenerateClick
    end
    inline fedOpt: TframEdt
      Left = -2
      Top = 124
      Width = 860
      Height = 52
      TabOrder = 3
      ExplicitLeft = -2
      ExplicitTop = 124
      ExplicitWidth = 860
      inherited edt: TLabeledEdit
        Width = 691
        OnExit = fedOptedtExit
        ExplicitWidth = 691
      end
      inherited btnShow: TButton
        Left = 700
        ExplicitLeft = 700
      end
      inherited btnCopy: TButton
        Left = 734
        ExplicitLeft = 734
      end
      inherited btnPaste: TButton
        Left = 766
        ExplicitLeft = 766
      end
      inherited btnImport: TButton
        Left = 798
        ExplicitLeft = 798
      end
      inherited btnExport: TButton
        Left = 830
        ExplicitLeft = 830
      end
    end
  end
  inline fedIn: TframEdt
    Left = 8
    Top = 8
    Width = 860
    Height = 52
    TabOrder = 5
    ExplicitLeft = 8
    ExplicitTop = 8
    ExplicitWidth = 860
    inherited edt: TLabeledEdit
      Width = 691
      ExplicitWidth = 691
    end
    inherited btnShow: TButton
      Left = 700
      ExplicitLeft = 700
    end
    inherited btnCopy: TButton
      Left = 734
      ExplicitLeft = 734
    end
    inherited btnPaste: TButton
      Left = 766
      ExplicitLeft = 766
    end
    inherited btnImport: TButton
      Left = 798
      ExplicitLeft = 798
    end
    inherited btnExport: TButton
      Left = 830
      ExplicitLeft = 830
    end
  end
  inline fedOut: TframEdt
    Left = 8
    Top = 481
    Width = 860
    Height = 52
    TabOrder = 6
    ExplicitLeft = 8
    ExplicitTop = 481
    ExplicitWidth = 860
    inherited edt: TLabeledEdit
      Width = 691
      ExplicitWidth = 691
    end
    inherited btnShow: TButton
      Left = 700
      ExplicitLeft = 700
    end
    inherited btnCopy: TButton
      Left = 734
      ExplicitLeft = 734
    end
    inherited btnPaste: TButton
      Left = 766
      ExplicitLeft = 766
    end
    inherited btnImport: TButton
      Left = 798
      ExplicitLeft = 798
    end
    inherited btnExport: TButton
      Left = 830
      ExplicitLeft = 830
    end
  end
  object mainMenu: TMainMenu
    Left = 64
    object view: TMenuItem
      Caption = 'View'
      object darkmode: TMenuItem
        Caption = 'Dark mode'
        Checked = True
        OnClick = darkmodeClick
      end
    end
    object help: TMenuItem
      Caption = 'Help'
      OnClick = helpClick
    end
  end
end
