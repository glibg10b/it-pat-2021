program en_decryptor_p;

uses
  Vcl.Forms,
  en_decryptor_u in 'en_decryptor_u.pas' {frmMain},
  frmHelp_u in 'frmHelp_u.pas' {frmHelp},
  frmLoading_u in 'frmLoading_u.pas' {frmLoading},
  framEdt_u in 'framEdt_u.pas' {framEdt: TFrame},
  strmanip_u in 'strmanip_u.pas',
  ciphers_u in 'ciphers_u.pas',
  algorithms_u in 'algorithms_u.pas',
  io_u in 'io_u.pas',
  theme_u in 'theme_u.pas';

{$R *.res}

begin
  Application.Initialize;
  
  loadTheme();
  
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmLoading, frmLoading);
  Application.CreateForm(TfrmHelp, frmHelp);
  Application.Run;

end.
