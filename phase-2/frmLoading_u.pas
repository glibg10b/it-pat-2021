unit frmLoading_u;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ComCtrls, Vcl.ExtCtrls,

  ciphers_u;

type
  TfrmLoading = class(TForm)
    pgbProgress: TProgressBar;
    lblStatus: TLabel;
    tmrLoadToggle: TTimer;
    procedure FormShow(Sender: TObject);
    procedure tmrLoadToggleTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    function enDeCrypt(sInput: string; cipher: TCipher; crypt: TCrypt): string;
  public
    { Public declarations }
  end;

var
  frmLoading: TfrmLoading;

implementation

{$R *.dfm}

uses en_decryptor_u, strmanip_u;

procedure TfrmLoading.FormCreate(Sender: TObject);
begin
end;

// If en-/decryption is done inside FormShow, the form won't show until
// it's done, and TForm.Close will be ignored. To get around this, we delegate
// the job to tmrLoadToggleTimer().
procedure TfrmLoading.FormShow(Sender: TObject);
begin
  tmrLoadToggle.enabled := true;
end;

// Reset the timer and call the en/decryption function
procedure TfrmLoading.tmrLoadToggleTimer(Sender: TObject);
begin
  tmrLoadToggle.enabled := false;

  with frmMain do
    fedOut.setText(enDeCrypt(fedIn.edt.text,
                                 TCipher(cbxCipher.itemIndex),
                                 TCrypt(rgpMode.itemIndex)));

  frmLoading.Close;
end;

// En/decrypts input text and shows it in the output text box
// cipher: The cipher to use
// crypt: encrypt or decrypt
function TfrmLoading.enDeCrypt(sInput: string; cipher: TCipher; crypt: TCrypt)
  : string;
begin
  result := sInput;

  with frmMain do
  begin
    pgbProgress.min := low(sInput) - 1;
    pgbProgress.max := high(sInput);
    pgbProgress.position := low(sInput) - 1;
    pgbProgress.step := 1;

    if crypt = crypt_encrypt then lblStatus.caption := 'En'
                             else lblStatus.caption := 'De';
    lblStatus.caption := lblStatus.caption + 'crypting with '
      + CIPHER_NAMES[cipher];

    if cipher = cipher_rot13 then rot13(result)
    else if rgpOpts.itemIndex = 0 then // Default
      case cipher of
        cipher_caesar: caesar(     result, crypt);
        cipher_shuf:   shufCipher( result, crypt);
        cipher_subst:  subst(      result, crypt);
        cipher_otp:    otp(        result);
      end
    else                               // Custom
      case cipher of
        cipher_caesar: caesar(     result, crypt, sedNum.value);
        cipher_shuf:   shufCipher( result, crypt, sedNum.value);
        cipher_subst:  subst(      result, crypt, fedOpt.edt.text);
        cipher_otp:    otp(        result,        fedOpt.edt.text);
      end;
  end;
end;

end.
