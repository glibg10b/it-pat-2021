object frmLoading: TfrmLoading
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'Form3'
  ClientHeight = 111
  ClientWidth = 434
  Color = clBtnFace
  Constraints.MaxHeight = 150
  Constraints.MaxWidth = 450
  Constraints.MinHeight = 150
  Constraints.MinWidth = 450
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  OnCreate = FormCreate
  OnShow = FormShow
  DesignSize = (
    434
    111)
  PixelsPerInch = 96
  TextHeight = 13
  object lblStatus: TLabel
    Left = 8
    Top = 35
    Width = 65
    Height = 13
    Caption = 'Doing stuff...'
  end
  object pgbProgress: TProgressBar
    Left = 8
    Top = 54
    Width = 418
    Height = 17
    Anchors = [akLeft, akRight]
    Max = 1000
    Smooth = True
    Step = 1
    TabOrder = 0
  end
  object tmrLoadToggle: TTimer
    Enabled = False
    Interval = 1
    OnTimer = tmrLoadToggleTimer
    Left = 200
    Top = 16
  end
end
