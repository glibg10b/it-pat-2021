// String manipulation functions

unit strmanip_u;

interface

// Returns true if empty.
function empty(sString: string): boolean;

// Shift cLetter by iNum, wrapping around if needed.
// Symbols are preserved.
procedure rot(var cLetter: char; iNum: integer);

implementation

uses math;

// s = '' is faster than length(s) > 0
function empty(sString: string): boolean;
begin
  result := sString = '';
end;

procedure rot(var cLetter: char; iNum: integer);
begin
  if cLetter in ['A'..'Z'] then
  begin
    inc(cLetter, iNum mod 26);
    if cLetter > 'Z' then dec(cLetter, 26);
  end
  else if cLetter in ['a'..'z'] then
  begin
    inc(cLetter, iNum mod 26);
    if cLetter > 'z' then dec(cLetter, 26);
  end;
end;
end.
