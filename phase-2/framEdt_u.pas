// A frame containing a label, a password text box and a few useful buttons
unit framEdt_u;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.Buttons;

type
  TframEdt = class(TFrame)
    edt: TLabeledEdit;
    btnShow: TButton;
    btnCopy: TButton;
    btnPaste: TButton;
    btnImport: TButton;
    btnExport: TButton;
    procedure btnShowMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnShowMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure btnCopyClick(Sender: TObject);
    procedure edtExit(Sender: TObject);
    procedure btnPasteClick(Sender: TObject);
    procedure setText(sInput: string);
    procedure enable(bEnable: boolean);
    procedure btnImportClick(Sender: TObject);
    procedure btnExportClick(Sender: TObject);
  private
    { Private declarations }
  public
    // Text buffer (preserves newlines and can hold more characters than TEdit)
    sText: string;
  end;

implementation

{$R *.dfm}

uses Clipbrd, io_u;

// Set the text in the buffer and the text box
procedure TframEdt.setText(sInput: string);
begin
  sText := sInput;
  edt.Text := sInput;
end;

// Enable/disable all components
procedure TframEdt.enable(bEnable: boolean);
begin
  edt.enabled       := bEnable;
  btnShow.enabled   := bEnable;
  btnCopy.enabled   := bEnable;
  btnPaste.enabled  := bEnable;
  btnImport.enabled := bEnable;
  btnExport.enabled := bEnable;
end;

// Copy text to the clipboard
procedure TframEdt.btnCopyClick(Sender: TObject);
begin
  clipboard.asText := sText;
end;

// Export the buffer to a file
procedure TframEdt.btnExportClick(Sender: TObject);
var
  sFile: string;
  tFile: textFile;
begin 
  try if not portTextFile(tFile, true) then exit except
    showMessage('Could not create/replace ' + sFile);
    exit;
  end;               

  writeln(tFile, sText);

  closeFile(tFile);
end;

// Import a file
procedure TframEdt.btnImportClick(Sender: TObject);
var
  sFile: string;
  tFile: textFile;
  sLine, sInput: string;
begin           
  try if not portTextFile(tFile, false) then exit except
    showMessage('Could not open ' + sFile);
    exit;
  end;
  
  sInput := '';
  while not eof(tFile) do
  begin
    readln(tFile, sLine);
    sInput := sInput + sLine + #10;
  end;

  closeFile(tFile);

  setText(sInput);
end;

// Paste the clipboard's contents
procedure TframEdt.btnPasteClick(Sender: TObject);
begin
  setText(clipboard.asText);
end;

// Reveal the text
procedure TframEdt.btnShowMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  edt.passwordChar := #0;
end;

// Hide the text
procedure TframEdt.btnShowMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  edt.passwordChar := '*';
end;

// Put the text in the buffer (onExit instead of onChange because it's slow)
procedure TframEdt.edtExit(Sender: TObject);
begin
  sText := edt.Text;
end;

end.
