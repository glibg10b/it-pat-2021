unit theme_u;

interface

procedure loadTheme();

implementation

uses Vcl.Themes, en_decryptor_u, System.SysUtils;

// Change the GUI theme according to the last line in the config file
// Must be called before the form is created, otherwise some components look
// weird
procedure loadTheme();
var
  tConfigFile: textFile;
  i: integer;
  sLine: string;
begin
  TStyleManager.TrySetStyle('Glossy');

  assignFile(tConfigFile, CONFIGFILE_NAME);
  try
    reset(tConfigFile);

    for i := 1 to 8 do readln(tConfigFile, sLine);
    if strToBool(sLine) then TStyleManager.TrySetStyle('Glossy')
                        else TStyleManager.TrySetStyle('Windows');

    closeFile(tConfigFile);
  except exit end;

end;

end.
