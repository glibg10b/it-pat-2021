// Algorithms

unit algorithms_u;

interface

// Sort the characters in sChars with quicksort (Hoare's partition scheme)
function quicksort(sChars: string; // String to sort
                   iLow: integer;  // Index of first element
                   iHigh: integer  // Index of last element
                   ): string;

// Fill sString with random letters
function randFill(sString: string): string;

// Shuffle the characters in sChars
function shuffle(sChars: string): string;

implementation

uses math;

// Returns the pivot (used by Quicksort)
function partition(var sChars: string; iLow: integer; iHigh: integer): integer;
var
  cPivot: char;           // Pivot value
  iLeft, iRight: integer; // Left and right indices
  cTemp: char;            // Used for swapping
begin
  cPivot := sChars[iLow + (iHigh - iLow) div 2];
  iLeft  := iLow - 1;
  iRight := iHigh + 1;

  while true do
  begin
    repeat inc(iLeft)  until sChars[iLeft]  >= cPivot;
    repeat dec(iRight) until sChars[iRight] <= cPivot;

    if iLeft >= iRight then break;

    cTemp          := sChars[iLeft];
    sChars[iLeft]  := sChars[iRight];
    sChars[iRight] := cTemp;
  end;

  result := iRight;
end;

// Quicksort has O(log n) space complexity so recursion is fine (the stack won't
// overflow, even with large arrays). See en.wikipedia.org/wiki/quicksort for
// how Quicksort works.
function quicksort(sChars: string; iLow: integer; iHigh: integer): string;
var iPivot: integer;
begin
  result := sChars;

  if (iLow >= 0) and (iHigh >= 0) and (iLow < iHigh) then
  begin
    iPivot := partition(result, iLow, iHigh);
    result := quicksort(result, iLow, iPivot);
    result := quicksort(result, iPivot + 1, iHigh);
  end;
end;

function randFill(sString: string): string;
var i: integer;
begin
  setLength(result, length(sString));

  for i := low(result) to high(result) do
    result[i] := char(randomRange(32, 126));
end;

// Uses modern Fisher-Yates algoritm
function shuffle(sChars: string): string;
var
  i, iRand: integer;
  cTemp: char;
begin
  result := sChars;

  for i := low(result) to high(result) - 1 do
  begin
    iRand := randomRange(i, High(result));
    cTemp         := result[i];
    result[i]     := result[iRand];
    result[iRand] := cTemp;
  end;
end;


end.
